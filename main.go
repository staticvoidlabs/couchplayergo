package main

import (
	"fmt"
	"time"

	Manager "./csManager"
	Models "./csModel"

	"github.com/veandco/go-sdl2/sdl"
)

var (
	currentConfig Models.Config
	showsRepo     Models.ShowsRepo
	selectedItem  int
	shouldExit    bool
	ticks         uint32
)

func main() {

	// Init config.
	currentConfig = Manager.GetCurrentConfig()

	// Init SDL.
	Manager.InitSdlSubsysten(&currentConfig.SdlConfig)

	/*
		// To be implemented...
		defer window.Destroy()
		defer surfaceImg.Free()
		defer renderer.Destroy()
	*/

	// Init shows repo.
	showsRepo = Manager.InitShowsRepo()
	var showsSDL = make([]Models.SimpleSdlShowItem, len(showsRepo.Shows))
	selectedItem = 0

	for i, s := range showsRepo.Shows {
		showsSDL[i].IsSelected = false
		showsSDL[i].IsContextMenuActive = false
		showsSDL[i].Surface, _ = Manager.FontShowItem.RenderUTF8Blended(s.Origin+": "+s.TitleShort, sdl.Color{200, 200, 200, 200})
		showsSDL[i].SurfaceSelected, _ = Manager.FontShowItem.RenderUTF8Blended(s.Origin+": "+s.TitleShort, sdl.Color{255, 255, 255, 255})
		showsSDL[i].SurfaceContext, _ = Manager.FontShowItem.RenderUTF8Blended("Löschen?", sdl.Color{204, 0, 0, 255})
		showsSDL[i].Texture, _ = Manager.Renderer.CreateTextureFromSurface(showsSDL[i].Surface)
		showsSDL[i].TextureSelected, _ = Manager.Renderer.CreateTextureFromSurface(showsSDL[i].SurfaceSelected)
		showsSDL[i].TextureContext, _ = Manager.Renderer.CreateTextureFromSurface(showsSDL[i].SurfaceContext)
	}

	shouldExit = false

	for {

		// Evaluate current event.
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {

			//switch event.(type) {
			switch t := event.(type) {

			case *sdl.QuitEvent:
				return

			case *sdl.KeyboardEvent:
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_ESCAPE {
					fmt.Println("Key: Esc")
					return
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_DOWN {
					fmt.Println("Key: Down")
					showsSDL[selectedItem].IsContextMenuActive = false
					selectedItem++
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_UP {
					fmt.Println("Key: Up")
					showsSDL[selectedItem].IsContextMenuActive = false
					selectedItem--
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_LEFT {
					fmt.Println("Key: Left")
					showsSDL[selectedItem].IsContextMenuActive = true
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_RIGHT {
					fmt.Println("Key: Right")
					showsSDL[selectedItem].IsContextMenuActive = false
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_f {
					fmt.Println("Key: f")
					shouldExit = true
					go Manager.StartPlayback(&showsRepo.Shows[selectedItem])
				}
			}

		}

		if shouldExit {
			fmt.Println("Will exit now...")
			time.Sleep(3 * time.Second)

			return
		}

		Manager.Renderer.SetDrawColor(10, 10, 10, 10)
		Manager.Renderer.Clear()

		// Render Header.
		surfaceHeader, _ := Manager.FontHeader.RenderUTF8Blended("Couch Player", sdl.Color{200, 200, 200, 200})
		textureHeader, _ := Manager.Renderer.CreateTextureFromSurface(surfaceHeader)
		_, _, tmpW, tmpH, _ := textureHeader.Query()

		Manager.Renderer.Copy(textureHeader, nil, &sdl.Rect{X: 10, Y: 10, W: tmpW, H: tmpH})

		// Render show textures.
		for i, s := range showsSDL {

			// Render background textrure.
			if s.IsContextMenuActive {
				_, _, tmpW, tmpH, _ := s.TextureContext.Query()
				tmpY := currentConfig.SdlConfig.OffsetHeader + (int32(i) * 25)
				Manager.Renderer.Copy(s.TextureContext, nil, &sdl.Rect{X: 20, Y: tmpY, W: tmpW, H: tmpH})
			} else {

				// Render foreground textrure.
				_, _, tmpW, tmpH, _ := s.Texture.Query()
				tmpY := currentConfig.SdlConfig.OffsetHeader + (int32(i) * 25)

				if i == selectedItem {
					Manager.Renderer.Copy(s.TextureSelected, nil, &sdl.Rect{X: 20, Y: tmpY, W: tmpW, H: tmpH})
				}
				Manager.Renderer.Copy(s.Texture, nil, &sdl.Rect{X: 20, Y: tmpY, W: tmpW, H: tmpH})
			}

			// Draw item background rect.
			//renderer.SetDrawColor(200, 200, 200, 255)
			//renderer.DrawRect(&sdl.Rect{X: 10, Y: tmpY, W: 350, H: tmpH})
		}

		Manager.Renderer.Present()
	}
}
